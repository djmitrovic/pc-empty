---
title: Network Architecture
excerpt: In this section you'll find basic info of your Network.
weight: 2
layout: docs
---
|
|web01   | dmz  | 210.0.0.0/24  | 210.0.0.1 | 
|firewall| dmz  | 210.0.0.0/24  | 210.0.0.2 | 
|db01    | internal | 172.0.0.0/24  | 172.0.0.1 | 
|dns     | internal | 172.0.0.0/24  | 172.0.0.2 |